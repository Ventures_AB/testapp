package com.museon.testapp.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.museon.testapp.BuildConfig;
import com.museon.testapp.R;
import com.museon.testapp.application.TestApp;
import com.museon.testapp.application.UserPreference;
import com.museon.testapp.constants.UrlConstants;
import com.museon.testapp.service.IonResult;
import com.museon.testapp.service.IonService;
import com.museon.testapp.service.NetworkAvailableCheck;
import com.museon.testapp.utils.AppLog;
import com.museon.testapp.utils.FileUtils;
import com.museon.testapp.utils.ViewUtils;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Aiswarya on 03/03/18.
 */

public class ProfileFragment extends Fragment implements View.OnClickListener{

    private static final String ARG_SECTION_NUMBER = "section_number";

    Activity mContext;
    private BottomSheetDialog imageChooserDialog;
    private ProgressDialog dlgProgress;
    private TextView nameText;
    private TextView mobileText;
    private ImageView profileImage;
    private AVLoadingIndicatorView imageLoadingIndicator;

    private static String profileImagePath;
    private static String profileImageName;
    private Uri profImgUri;
    private Uri cameraImageUri;

    public static final int CHOOSE_BROWSER_IMAGE_CODE = 607;
    public static final int CHOOSE_BROWSER_CAMERA_CODE = 976;
    private static final int REQUEST_STORAGE_PERMISSION = 786;
    private static final String NO_IMAGE_FLAG = "NoImage";

    private String TAG = "PROFILE";

    public ProfileFragment() {
    }

    public static ProfileFragment newInstance(int sectionNumber) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            getMyProfile();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_profile, container, false);

        mContext = getActivity();
        nameText = (TextView) rootView.findViewById(R.id.name_txt);
        mobileText = (TextView) rootView.findViewById(R.id.mobile_txt);
        profileImage = (ImageView) rootView.findViewById(R.id.profile_img);
        imageLoadingIndicator = (AVLoadingIndicatorView) rootView.findViewById(R.id.progress_bar);

        Drawable ic_edit_button = VectorDrawableCompat.create(mContext.getResources(), R.drawable.ic_edit_profile_image, null);
        ((ImageView) rootView.findViewById(R.id.edit_profile_pic_btn)).setImageDrawable(ic_edit_button);

        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Ubuntu-R.ttf");
        Typeface boldTypeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Ubuntu-M.ttf");
        nameText.setTypeface(boldTypeface);
        mobileText.setTypeface(typeface);

        rootView.findViewById(R.id.edit_profile_pic_btn).setOnClickListener(this);

        return rootView;
    }

    private void getMyProfile() {
        try {

            if (!NetworkAvailableCheck.isNetworkAvailable(mContext)) {
                Toast.makeText(mContext, R.string.please_connect_to_network, Toast.LENGTH_SHORT).show();
            }
            dlgProgress = ViewUtils.getAppCommonProgressBar(mContext);
            ViewUtils.showProgressDialog(mContext, dlgProgress);

            String url = UrlConstants.BASE_URL + UrlConstants.CONTACTS_API;
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("mobile", UserPreference.getInstance().getMyNumber(mContext));
            AppLog.d(TestApp.TAG, TAG + " Get profile url : " +url +
                    "\nGet profile post params : " +UserPreference.getInstance().getMyNumber(mContext));

            new IonService(new IonResult() {
                @Override
                public void notifyResponseSuccess(int status, String message, JsonObject result) {
                    try {
                        AppLog.d(TestApp.TAG, TAG+ " Get profile Response : " +result);

                        if (status == 202) {

                            JsonArray jsonArray = result.get("list").getAsJsonArray();
                            if (jsonArray.size() > 0) {

                                String myNumber = UserPreference.getInstance().getMyNumber(mContext);

                                for (int i = 0; i < jsonArray.size(); i++) {

                                    JsonObject response = jsonArray.get(i).getAsJsonObject();

                                    if (myNumber.equalsIgnoreCase(response.get("mobile").getAsString())) {

                                        nameText.setText(response.get("name").getAsString());
                                        mobileText.setText(response.get("mobile").getAsString());
                                        String profilePic = response.get("profile_pic").getAsString();

                                        if (!profilePic.equalsIgnoreCase("")) {
                                            ViewUtils.setProfileImageTestApp(profilePic, profileImage, imageLoadingIndicator);
                                        }
                                        ViewUtils.hideProgressDialog(mContext, dlgProgress);
                                        return;

                                    }

                                }
                            } else {
                                Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    ViewUtils.hideProgressDialog(mContext, dlgProgress);
                }

                @Override
                public void notifyResponseError(String message, Exception e) {
                    try {
                        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
                        Log.e(TestApp.TAG, TAG+ " Get profile Ion callback Error : " +e.getMessage());
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    ViewUtils.hideProgressDialog(mContext, dlgProgress);
                }

                @Override
                public void notifyNetworkError(String message) {
                    try {
                        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    ViewUtils.hideProgressDialog(mContext, dlgProgress);
                }
            }, getActivity()).postDataIon(url, jsonObject);

        } catch (Exception e) {
            Log.e(TestApp.TAG, TAG+ " Get Profile API call exception : " +e.getMessage());
            ViewUtils.hideProgressDialog(mContext, dlgProgress);
        }
    }

    private void updateProfile() {
        try {
            if (!NetworkAvailableCheck.isNetworkAvailable(getActivity())) {
                Toast.makeText(getActivity(), R.string.please_connect_to_network, Toast.LENGTH_SHORT).show();
            }
            dlgProgress = ViewUtils.getAppCommonProgressBar(mContext);
            ViewUtils.showProgressDialog(mContext, dlgProgress);

            String image_path = "";
            if (profImgUri != null){
                image_path = FileUtils.getPath(mContext, profImgUri);
            }

            String url = UrlConstants.BASE_URL + UrlConstants.PROFILE_UPDATE_API;

            AppLog.d(TestApp.TAG, TAG+" Update profile Url : " +url+
                    "\nUpdate profile post params : " +"userfile:" +image_path +"\n"
                    +"mobile:" +mobileText.getText().toString());

            new IonService(new IonResult() {
                @Override
                public void notifyResponseSuccess(int status, String message, JsonObject result) {

                    ViewUtils.hideProgressDialog(mContext, dlgProgress);
                    try {
                        AppLog.d(TestApp.TAG, TAG+ " Update profile Response : " +result);

                        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void notifyResponseError(String message, Exception e) {
                    try {
                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        Log.e(TestApp.TAG, TAG+ " Update profile Ion callback Error : " +e.getMessage());
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    ViewUtils.hideProgressDialog(mContext, dlgProgress);
                }

                @Override
                public void notifyNetworkError(String message) {
                    try {
                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    ViewUtils.hideProgressDialog(mContext, dlgProgress);
                }
            }, getActivity()).postUpdateProfileIon(url, mobileText.getText().toString().trim(), image_path);

        } catch (Exception e) {
            Log.e(TestApp.TAG, TAG+ " Update profile API call exception : " +e.getMessage());
            ViewUtils.hideProgressDialog(mContext, dlgProgress);
        }
    }

    private void initializeImageChooserBottomSheet() {
        try {
            final View bottomSheetView = getLayoutInflater().inflate(R.layout.view_bottom_sheet_image_picker, ViewUtils.getNullParent());
            imageChooserDialog = new BottomSheetDialog(mContext);
            imageChooserDialog.setContentView(bottomSheetView);
            imageChooserDialog.setCanceledOnTouchOutside(true);
            imageChooserDialog.setCancelable(true);
            final BottomSheetBehavior mBehavior = BottomSheetBehavior.from((View) bottomSheetView.getParent());
            imageChooserDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialog) {
                    mBehavior.setPeekHeight(bottomSheetView.getHeight());
                }
            });
            //Change bottom sheet color to transparent
            ((View) bottomSheetView.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));

            Drawable ic_camera = VectorDrawableCompat.create(mContext.getResources(), R.drawable.ic_bottom_sheet_camera, null);
            Drawable ic_gallery = VectorDrawableCompat.create(mContext.getResources(), R.drawable.ic_bottom_sheet_gallery, null);
            Drawable ic_remove_image = VectorDrawableCompat.create(mContext.getResources(), R.drawable.ic_bottom_sheet_no_image, null);
            ((ImageView) bottomSheetView.findViewById(R.id.BottomSheetImagePickerCameraImage)).setImageDrawable(ic_camera);
            ((ImageView) bottomSheetView.findViewById(R.id.BottomSheetImagePickerGalleryImage)).setImageDrawable(ic_gallery);
            ((ImageView) bottomSheetView.findViewById(R.id.BottomSheetImagePickerNoImageImage)).setImageDrawable(ic_remove_image);


            //Camera picker button click
            bottomSheetView.findViewById(R.id.BottomSheetCameraLayout).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if (imageChooserDialog != null && imageChooserDialog.isShowing()) {
                            imageChooserDialog.dismiss();
                        }
                        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        File photo = new File(FileUtils.getAppFileDirectory(), "profile_img_" + new SimpleDateFormat("yyyyMMddHHmmss", Locale.ENGLISH).format(new Date()) + ".jpg");
                        cameraImageUri = Uri.fromFile(photo);
                        Uri intentUri = FileProvider.getUriForFile(mContext, BuildConfig.APPLICATION_ID + ".provider", photo);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, intentUri);
                        startActivityForResult(Intent.createChooser(intent, "Open Camera Using"), CHOOSE_BROWSER_CAMERA_CODE);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });


            //Gallery picker button click
            bottomSheetView.findViewById(R.id.BottomSheetGalleryLayout).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        if (imageChooserDialog != null && imageChooserDialog.isShowing()) {
                            imageChooserDialog.dismiss();
                        }
                        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        i.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        i.setType("image/*");
                        startActivityForResult(i, CHOOSE_BROWSER_IMAGE_CODE);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Open pickers in bottom view
    private void openImageChooser() {
        if (imageChooserDialog != null && !imageChooserDialog.isShowing()) {
            imageChooserDialog.show();
        } else {
            initializeImageChooserBottomSheet();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            switch (requestCode) {
                case CHOOSE_BROWSER_IMAGE_CODE:
                    if (resultCode == RESULT_OK) {

                        profImgUri = data.getData();
                        setEditProfileImage();
                    }
                    break;
                case CHOOSE_BROWSER_CAMERA_CODE:
                    if (resultCode == RESULT_OK) {

                        profImgUri = cameraImageUri;
                        setEditProfileImage();
                    }
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void requestPermission(int request) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (request == REQUEST_STORAGE_PERMISSION) {//read contacts

                requestPermissions(new String[]{
                                Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        REQUEST_STORAGE_PERMISSION);

            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {

            if (requestCode == REQUEST_STORAGE_PERMISSION) {

                if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    AppLog.d(TestApp.TAG, "!!! Storage permission denied !!!");
                } else {
                    openImageChooser();
                }

            }
        }
    }

    //Add image in profile image view
    private void setEditProfileImage() {
        try {
            Glide.with(TestApp.appContext)
                    .load(profImgUri)
                    .thumbnail(1.0f)
                    .crossFade()
                    .fitCenter()
                    .override(200, 200)
                    .placeholder(R.mipmap.profile)
                    .error(R.mipmap.profile)
                    .skipMemoryCache(true)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .bitmapTransform(new CropCircleTransformation(TestApp.appContext))
                    .into(profileImage);
        } catch (Exception e) {
            e.printStackTrace();
        }
        updateProfile();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.edit_profile_pic_btn:
                if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermission(REQUEST_STORAGE_PERMISSION);
                } else {
                    openImageChooser();
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        ViewUtils.hideProgressDialog(mContext, dlgProgress);
        super.onDestroyView();
    }

}


