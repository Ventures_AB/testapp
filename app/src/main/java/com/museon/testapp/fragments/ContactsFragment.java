package com.museon.testapp.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.museon.testapp.R;
import com.museon.testapp.constants.UrlConstants;
import com.museon.testapp.adapter.ContactsAdapter;
import com.museon.testapp.application.TestApp;
import com.museon.testapp.model.Contacts;
import com.museon.testapp.service.IonResult;
import com.museon.testapp.service.IonService;
import com.museon.testapp.service.NetworkAvailableCheck;
import com.museon.testapp.service.RecyclerViewOnItemClickListener;
import com.museon.testapp.utils.AppLog;
import com.museon.testapp.utils.ViewUtils;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Aiswarya on 03/03/18.
 */

public class ContactsFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";

    Activity mContext;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView mContactsListView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private LinearLayout emptyListView;
    private ProgressDialog dlgProgress;
    private TextView errorText;

    private ArrayList<Contacts> mContacts;
    private HashMap<String, Contacts> mHashmapContactsList;
    private ContactsAdapter mContactsAdapter;

    private boolean isRefreshing = false;

    private static final int REQUEST_CONTACT_PERMISSION = 787;

    private String TAG = "CONTACTS";

    public ContactsFragment() {
    }

    public static ContactsFragment newInstance(int sectionNumber) {
        ContactsFragment fragment = new ContactsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_contact, container, false);

        mContext = getActivity();

        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.listSwipeRefreshLayout);
        mContactsListView = (RecyclerView) rootView.findViewById(R.id.listView);
        emptyListView = (LinearLayout) rootView.findViewById(R.id.emptyListView);
        errorText = (TextView) rootView.findViewById(R.id.errorTxt);

        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimaryDark);

        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Ubuntu-R.ttf");
        errorText.setTypeface(typeface);

        mContactsListView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mContactsListView.setLayoutManager(mLayoutManager);

        mContacts = new ArrayList<Contacts>();
        mHashmapContactsList = new HashMap<String, Contacts>();
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            requestPermission(REQUEST_CONTACT_PERMISSION);
        } else {
            getContacts();
        }

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isRefreshing = true;
                if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                    requestPermission(REQUEST_CONTACT_PERMISSION);
                } else {
                    getContacts();
                }
            }
        });

        return rootView;
    }

    private void getContacts() {
        try {

            if (!NetworkAvailableCheck.isNetworkAvailable(mContext)) {
                Toast.makeText(mContext, R.string.please_connect_to_network, Toast.LENGTH_SHORT).show();
            }
            dlgProgress = ViewUtils.getAppCommonProgressBar(mContext);
            ViewUtils.showProgressDialog(mContext, dlgProgress);

            String url = UrlConstants.BASE_URL + UrlConstants.CONTACTS_API;
            AppLog.d(TestApp.TAG, TAG + " url : " +url +
                    "\npost params : ");

            new IonService(new IonResult() {
                @Override
                public void notifyResponseSuccess(int status, String message, JsonObject result) {
                    try {
                        AppLog.d(TestApp.TAG, TAG+ " Response : " +result);

                        if (status == 202) {
                            JsonArray jsonArray = result.get("list").getAsJsonArray();
                            if (jsonArray.size() > 0) {
                                mContacts.clear();
                                mHashmapContactsList.clear();
                                for (int i = 0; i < jsonArray.size(); i++) {

                                    JsonObject response = jsonArray.get(i).getAsJsonObject();

                                    Contacts contact = new Contacts();
                                    contact.setName(response.get("name").getAsString());
                                    contact.setMobile(response.get("mobile").getAsString());
                                    contact.setProfile_pic(response.get("profile_pic").getAsString());
                                    //mContacts.add(contact);
                                    mHashmapContactsList.put(contact.getMobile(), contact);
                                }
                                getPhoneContacts();
                            } else {
                                errorText.setText(message);
                                setAdapter();
                            }
                        } else {
                            errorText.setText(message);
                            setAdapter();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    ViewUtils.hideProgressDialog(mContext, dlgProgress);
                }

                @Override
                public void notifyResponseError(String message, Exception e) {
                    try {
                        errorText.setText(message);
                        setAdapter();
                        Log.e(TestApp.TAG, TAG+ " Ion callback Error : " +e.getMessage());
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    ViewUtils.hideProgressDialog(mContext, dlgProgress);
                }

                @Override
                public void notifyNetworkError(String message) {
                    try {
                        errorText.setText(message);
                        setAdapter();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    ViewUtils.hideProgressDialog(mContext, dlgProgress);
                }
            }, getActivity()).postDataIon(url, new JsonObject());

        } catch (Exception e) {
            Log.e(TestApp.TAG, TAG+ " API call exception : " +e.getMessage());
            ViewUtils.hideProgressDialog(mContext, dlgProgress);
        }
        mSwipeRefreshLayout.setRefreshing(false);
        isRefreshing = false;
    }

    private void getPhoneContacts(){

        mContacts.clear();

        Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String[] projection = new String[] {ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER};

        Cursor people = getActivity().getContentResolver().query(uri, projection, null, null, null);

        int indexName = people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
        int indexNumber = people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);

        int j_count=0;
        String name;
        String number;
        people.moveToFirst();
        do {
            name = people.getString(indexName);
            number = people.getString(indexNumber);

            if (mHashmapContactsList.containsKey(number)){

                Contacts contact = new Contacts();
                contact.setProfile_pic(mHashmapContactsList.get(number).getProfile_pic());
                contact.setMobile(mHashmapContactsList.get(number).getMobile());

                if (name.trim().equalsIgnoreCase("")) {
                    contact.setName(mHashmapContactsList.get(number).getName());
                } else {
                    contact.setName(name);
                }
                mContacts.add(contact);
            }
            j_count++;

            // Do work...
        } while (people.moveToNext());

        setAdapter();
    }

    private void setAdapter() {

        mContactsAdapter = new ContactsAdapter(getActivity(), mContacts, contactOnItemClickListener);
        mContactsListView.setAdapter(mContactsAdapter);
        mContactsAdapter.notifyDataSetChanged();

        if (mContacts.size() > 0) {
            emptyListView.setVisibility(View.GONE);
        } else {
            emptyListView.setVisibility(View.VISIBLE);
        }
    }

    RecyclerViewOnItemClickListener contactOnItemClickListener = new RecyclerViewOnItemClickListener() {
        @Override
        public void onItemClick(View v, int position) {

            Contacts contact = mContacts.get(position);
            showContactDetails(contact);

        }
    };

    private void showContactDetails(Contacts contact) {

        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.dialog_contact_profile, null);

        ((TextView) alertLayout.findViewById(R.id.contact_name_txt)).setText(contact.getName());
        ((TextView) alertLayout.findViewById(R.id.contact_mobile_txt)).setText(contact.getMobile());

        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Ubuntu-R.ttf");
        Typeface boldTypeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Ubuntu-M.ttf");
        ((TextView) alertLayout.findViewById(R.id.contact_name_txt)).setTypeface(boldTypeface);
        ((TextView) alertLayout.findViewById(R.id.contact_mobile_txt)).setTypeface(typeface);

        if (!contact.getProfile_pic().equalsIgnoreCase("")) {
            ViewUtils.setProfileImageTestApp(contact.getProfile_pic(), ((ImageView) alertLayout.findViewById(R.id.contact_img)),
                    ((AVLoadingIndicatorView) alertLayout.findViewById(R.id.progress_bar)));
        }

        alert.setView(alertLayout);
        alert.setCancelable(true);
        alert.setNegativeButton("", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        alert.setPositiveButton("", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        AlertDialog dialog = alert.create();
        dialog.show();
    }

    public void requestPermission(int request) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (request == REQUEST_CONTACT_PERMISSION) {//read contacts

                requestPermissions(new String[]{
                                Manifest.permission.READ_CONTACTS},
                        REQUEST_CONTACT_PERMISSION);

            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {

            if (requestCode == REQUEST_CONTACT_PERMISSION) {

                if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    AppLog.d(TestApp.TAG, "!!! Read contact permission denied !!!");
                }

            }
        }
    }

    @Override
    public void onDestroyView() {
        ViewUtils.hideProgressDialog(mContext, dlgProgress);
        super.onDestroyView();
    }
}


