package com.museon.testapp.service;

import android.content.Context;
import android.util.Log;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.museon.testapp.R;
import com.museon.testapp.application.TestApp;

import java.io.File;

/**
 * Created by Aiswarya on 04/03/18.
 */

public class IonService {

    IonResult mResultCallback = null;
    Context mContext;

    public IonService(IonResult resultCallback, Context context){
        mResultCallback = resultCallback;
        mContext = context;
    }

    public void postDataIon(String url, JsonObject jsonObject) {
        try {
            if (NetworkAvailableCheck.isNetworkAvailable(mContext)) {

                Ion.with(mContext)
                        .load(url)
                        .setJsonObjectBody(jsonObject)
                        .asJsonObject()
                        .setCallback(new FutureCallback<JsonObject>() {
                            @Override
                            public void onCompleted(Exception e, JsonObject result) {

                                try {
                                    if (e != null) {
                                        mResultCallback.notifyResponseError(mContext.getResources().getString(R.string.server_connection_error), e);
                                    } else {

                                        int status = result.get("status").getAsInt();
                                        String message = result.get("msg").getAsString();
                                        mResultCallback.notifyResponseSuccess(status, message, result);
                                    }
                                } catch (Exception e1) {
                                    mResultCallback.notifyResponseError(mContext.getResources().getString(R.string.network_request_failed), e1);
                                }
                            }
                        });
            } else {
                mResultCallback.notifyNetworkError(mContext.getResources().getString(R.string.please_connect_to_network));
            }
        } catch (Exception ex) {
            mResultCallback.notifyResponseError(mContext.getResources().getString(R.string.network_request_failed), ex);
        }
    }

    public void postSignUpIon(String url, String name, String mobile, String uploadImagePath) {
        try {
            if (NetworkAvailableCheck.isNetworkAvailable(mContext)) {

                File fileToUpload = new File(uploadImagePath);

                Ion.with(mContext)
                        .load(url)
                        .setLogging(TestApp.TAG, Log.DEBUG)
                        .setMultipartParameter("name", name)
                        .setMultipartParameter("mobile", mobile)
                        .setMultipartFile("userfile", "image/*", fileToUpload)
                        .asJsonObject()
                        .setCallback(new FutureCallback<JsonObject>() {
                            @Override
                            public void onCompleted(Exception e, JsonObject result) {

                                try {
                                    if (e != null) {
                                        mResultCallback.notifyResponseError(mContext.getResources().getString(R.string.server_connection_error), e);
                                    } else {

                                        int status = result.get("status").getAsInt();
                                        String message = result.get("msg").getAsString();
                                        mResultCallback.notifyResponseSuccess(status, message, result);
                                    }
                                } catch (Exception e1) {
                                    mResultCallback.notifyResponseError(mContext.getResources().getString(R.string.network_request_failed), e1);
                                }
                            }
                        });
            } else {
                mResultCallback.notifyNetworkError(mContext.getResources().getString(R.string.please_connect_to_network));
            }
        } catch (Exception ex) {
            mResultCallback.notifyResponseError(mContext.getResources().getString(R.string.network_request_failed), ex);
        }
    }

    public void postUpdateProfileIon(String url, String mobile, String uploadImagePath) {
        try {
            if (NetworkAvailableCheck.isNetworkAvailable(mContext)) {

                File fileToUpload = new File(uploadImagePath);

                Ion.with(mContext)
                        .load(url)
                        .setLogging(TestApp.TAG, Log.DEBUG)
                        .setMultipartParameter("mobile", mobile)
                        .setMultipartFile("userfile", "image/*", fileToUpload)
                        .asJsonObject()
                        .setCallback(new FutureCallback<JsonObject>() {
                            @Override
                            public void onCompleted(Exception e, JsonObject result) {

                                try {
                                    if (e != null) {
                                        mResultCallback.notifyResponseError(mContext.getResources().getString(R.string.server_connection_error), e);
                                    } else {

                                        int status = result.get("status").getAsInt();
                                        String message = result.get("msg").getAsString();
                                        mResultCallback.notifyResponseSuccess(status, message, result);
                                    }
                                } catch (Exception e1) {
                                    mResultCallback.notifyResponseError(mContext.getResources().getString(R.string.network_request_failed), e1);
                                }
                            }
                        });
            } else {
                mResultCallback.notifyNetworkError(mContext.getResources().getString(R.string.please_connect_to_network));
            }
        } catch (Exception ex) {
            mResultCallback.notifyResponseError(mContext.getResources().getString(R.string.network_request_failed), ex);
        }
    }

    public void getDataIon(String url) {
        try {
            if (NetworkAvailableCheck.isNetworkAvailable(mContext)) {

                Ion.with(mContext)
                        .load(url)
                        .asJsonObject()
                        .setCallback(new FutureCallback<JsonObject>() {
                            @Override
                            public void onCompleted(Exception e, JsonObject result) {

                                try {
                                    if (e != null) {
                                        mResultCallback.notifyResponseError(mContext.getResources().getString(R.string.server_connection_error), e);
                                    } else {
                                        int status = result.get("status").getAsInt();
                                        String message = result.get("msg").getAsString();
                                        mResultCallback.notifyResponseSuccess(status, message, result);
                                    }
                                } catch (Exception e1) {
                                    mResultCallback.notifyResponseError(mContext.getResources().getString(R.string.network_request_failed), e1);
                                }
                            }
                        });
            } else {
                mResultCallback.notifyNetworkError(mContext.getResources().getString(R.string.please_connect_to_network));
            }
        } catch (Exception ex) {
            mResultCallback.notifyResponseError(mContext.getResources().getString(R.string.network_request_failed), ex);
        }
    }
}
