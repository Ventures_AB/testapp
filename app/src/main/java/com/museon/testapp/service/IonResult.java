package com.museon.testapp.service;

import com.google.gson.JsonObject;

/**
 * Created by Aiswarya on 04/03/18.
 */

public interface IonResult {

    public void notifyResponseSuccess(int status, String message, JsonObject result);
    public void notifyResponseError(String message, Exception e);
    public void notifyNetworkError(String message);
}
