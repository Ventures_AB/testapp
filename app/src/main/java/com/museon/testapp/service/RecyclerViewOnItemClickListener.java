package com.museon.testapp.service;

import android.view.View;

/**
 * Created by Aiswarya on 04/03/18.
 */

public interface RecyclerViewOnItemClickListener {

    public void onItemClick(View v, int position);
}
