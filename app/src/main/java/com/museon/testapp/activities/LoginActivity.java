package com.museon.testapp.activities;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.JsonObject;
import com.museon.testapp.BuildConfig;
import com.museon.testapp.R;
import com.museon.testapp.application.TestApp;
import com.museon.testapp.application.UserPreference;
import com.museon.testapp.constants.UrlConstants;
import com.museon.testapp.service.IonResult;
import com.museon.testapp.service.IonService;
import com.museon.testapp.service.NetworkAvailableCheck;
import com.museon.testapp.utils.AppLog;
import com.museon.testapp.utils.FileUtils;
import com.museon.testapp.utils.ViewUtils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{

    private Activity mContext;
    private EditText nameEditText;
    private EditText mobileEditText;
    private ProgressDialog dlgProgress;
    private BottomSheetDialog imageChooserDialog;
    private static String profileImagePath;
    private static String profileImageName;

    private Uri profImgUri = null;

    private Uri cameraImageUri;

    public static final int CHOOSE_BROWSER_IMAGE_CODE = 607;
    public static final int CHOOSE_BROWSER_CAMERA_CODE = 976;
    private static final int REQUEST_CODE_PERMISSION = 706;
    private static final int REQUEST_STORAGE_PERMISSION = 786;
    private static final String NO_IMAGE_FLAG = "NoImage";

    private String TAG = "REGISTER";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mContext = this;
        nameEditText = (EditText) findViewById(R.id.name_txt);
        mobileEditText = (EditText) findViewById(R.id.mobile_txt);

        Drawable ic_logo = VectorDrawableCompat.create(mContext.getResources(), R.drawable.ic_logo, null);
        ((ImageView) findViewById(R.id.logo)).setImageDrawable(ic_logo);

        requestPermission(REQUEST_CODE_PERMISSION);

        mobileEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    validation();
                    return true;
                }
                return false;
            }
        });

        findViewById(R.id.prof_img).setOnClickListener(this);
        findViewById(R.id.start_btn).setOnClickListener(this);
    }


    private void register() {
        try {
            if (!NetworkAvailableCheck.isNetworkAvailable(getApplicationContext())) {
                Toast.makeText(getApplicationContext(), R.string.please_connect_to_network, Toast.LENGTH_SHORT).show();
            }
            dlgProgress = ViewUtils.getAppCommonProgressBar(mContext);
            ViewUtils.showProgressDialog(mContext, dlgProgress);

            String image_path = "";
            if (profImgUri != null){
                image_path = FileUtils.getPath(mContext, profImgUri);
            }

            String url = UrlConstants.BASE_URL + UrlConstants.REGISTER_API;

            AppLog.d(TestApp.TAG, TAG+" Url : " +url+
                    "\npost params : " +"userfile:" +image_path +"\n"
                    +"name:" +nameEditText.getText().toString().trim() +"\n"
                    +"mobile:" +mobileEditText.getText().toString());

            new IonService(new IonResult() {
                @Override
                public void notifyResponseSuccess(int status, String message, JsonObject result) {

                    ViewUtils.hideProgressDialog(mContext, dlgProgress);
                    try {
                        AppLog.d(TestApp.TAG, TAG+ " Response : " +result);

                        if (status == 202) {
                            Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();

                            UserPreference.getInstance().saveMyNumber(mContext, mobileEditText.getText().toString().trim());
                            Intent intent = new Intent(mContext, MainActivity.class);
                            startActivity(intent);
                            finish();

                        } else if (status == 204) {
                            mobileEditText.setError(message);
                        } else {
                            Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void notifyResponseError(String message, Exception e) {
                    try {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        Log.e(TestApp.TAG, TAG+ " Ion callback Error : " +e.getMessage());
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    ViewUtils.hideProgressDialog(mContext, dlgProgress);
                }

                @Override
                public void notifyNetworkError(String message) {
                    try {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    ViewUtils.hideProgressDialog(mContext, dlgProgress);
                }
            }, getApplicationContext()).postSignUpIon(url, nameEditText.getText().toString().trim(), mobileEditText.getText().toString().trim(),
                    image_path);

        } catch (Exception e) {
            Log.e(TestApp.TAG, TAG+ " API call exception : " +e.getMessage());
            ViewUtils.hideProgressDialog(mContext, dlgProgress);
        }
    }

    //Adding pickers in bottom view
    private void initializeImageChooserBottomSheet() {
        try {
            final View bottomSheetView = getLayoutInflater().inflate(R.layout.view_bottom_sheet_image_picker, ViewUtils.getNullParent());
            imageChooserDialog = new BottomSheetDialog(mContext);
            imageChooserDialog.setContentView(bottomSheetView);
            imageChooserDialog.setCanceledOnTouchOutside(true);
            imageChooserDialog.setCancelable(true);
            final BottomSheetBehavior mBehavior = BottomSheetBehavior.from((View) bottomSheetView.getParent());
            imageChooserDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialog) {
                    mBehavior.setPeekHeight(bottomSheetView.getHeight());
                }
            });
            //Change bottom sheet color to transparent
            ((View) bottomSheetView.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));

            Drawable ic_camera = VectorDrawableCompat.create(mContext.getResources(), R.drawable.ic_bottom_sheet_camera, null);
            Drawable ic_gallery = VectorDrawableCompat.create(mContext.getResources(), R.drawable.ic_bottom_sheet_gallery, null);
            Drawable ic_remove_image = VectorDrawableCompat.create(mContext.getResources(), R.drawable.ic_bottom_sheet_no_image, null);
            ((ImageView) bottomSheetView.findViewById(R.id.BottomSheetImagePickerCameraImage)).setImageDrawable(ic_camera);
            ((ImageView) bottomSheetView.findViewById(R.id.BottomSheetImagePickerGalleryImage)).setImageDrawable(ic_gallery);
            ((ImageView) bottomSheetView.findViewById(R.id.BottomSheetImagePickerNoImageImage)).setImageDrawable(ic_remove_image);

            //Camera picker button click
            bottomSheetView.findViewById(R.id.BottomSheetCameraLayout).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if (imageChooserDialog != null && imageChooserDialog.isShowing()) {
                            imageChooserDialog.dismiss();
                        }
                        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        File photo = new File(FileUtils.getAppFileDirectory(), "profile_img_" + new SimpleDateFormat("yyyyMMddHHmmss", Locale.ENGLISH).format(new Date()) + ".jpg");
                        cameraImageUri = Uri.fromFile(photo);
                        Uri intentUri = FileProvider.getUriForFile(mContext, BuildConfig.APPLICATION_ID + ".provider", photo);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, intentUri);
                        startActivityForResult(Intent.createChooser(intent, "Open Camera Using"), CHOOSE_BROWSER_CAMERA_CODE);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });

            //Gallery picker button click
            bottomSheetView.findViewById(R.id.BottomSheetGalleryLayout).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        if (imageChooserDialog != null && imageChooserDialog.isShowing()) {
                            imageChooserDialog.dismiss();
                        }
                        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        i.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        i.setType("image/*");
                        startActivityForResult(i, CHOOSE_BROWSER_IMAGE_CODE);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Open pickers in bottom view
    private void openImageChooser() {
        if (imageChooserDialog != null && !imageChooserDialog.isShowing()) {
            imageChooserDialog.show();
        } else {
            initializeImageChooserBottomSheet();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            switch (requestCode) {
                case CHOOSE_BROWSER_IMAGE_CODE:
                    if (resultCode == RESULT_OK) {

                        profImgUri = data.getData();
                        setEditProfileImage();
                    }
                    break;
                case CHOOSE_BROWSER_CAMERA_CODE:
                    if (resultCode == RESULT_OK) {

                        profImgUri = cameraImageUri;
                        setEditProfileImage();
                    }
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void requestPermission(int request) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (request == REQUEST_CODE_PERMISSION) {//both storage and read contacts

                requestPermissions(new String[]{
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_CONTACTS,},
                        REQUEST_CODE_PERMISSION);

            } else if (request == REQUEST_STORAGE_PERMISSION) {//read contacts

                requestPermissions(new String[]{
                                Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        REQUEST_STORAGE_PERMISSION);

            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {

            if (requestCode == REQUEST_CODE_PERMISSION) {

                if (grantResults[0] == PackageManager.PERMISSION_DENIED && grantResults[1] == PackageManager.PERMISSION_DENIED) {
                    AppLog.d(TestApp.TAG, "!!! Both storage & read contacts permission denied !!!");
                } else if (grantResults[0] == PackageManager.PERMISSION_DENIED){
                    AppLog.d(TestApp.TAG, "!!! Storage permission denied !!!");
                } else if (grantResults[1] == PackageManager.PERMISSION_DENIED){
                    AppLog.d(TestApp.TAG, "!!! Read contacts permission denied !!!");
                }
            } else if (requestCode == REQUEST_STORAGE_PERMISSION) {

                if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    AppLog.d(TestApp.TAG, "!!! Storage permission denied !!!");
                } else {
                    openImageChooser();
                }

            }
        }
    }

    //Add image in profile image view
    private void setEditProfileImage() {
        try {
            Glide.with(TestApp.appContext)
                    .load(profImgUri)
                    .thumbnail(1.0f)
                    .crossFade()
                    .fitCenter()
                    .override(200, 200)
                    .placeholder(R.mipmap.profile)
                    .error(R.mipmap.profile)
                    .skipMemoryCache(true)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .bitmapTransform(new CropCircleTransformation(TestApp.appContext))
                    .into(((ImageView) findViewById(R.id.prof_img)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isValidCellPhone() {
        return android.util.Patterns.PHONE.matcher(mobileEditText.getText().toString().trim()).matches();
    }

    private void validation() {

        try {
            InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm.isAcceptingText()) {
                InputMethodManager inputManager = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(mContext.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        boolean isFormContentValid = true;

        if(nameEditText.getText().toString().trim().equalsIgnoreCase("")){
            nameEditText.setError("Cannot Be Blank");
            isFormContentValid = false;
        }else {
            nameEditText.setError(null);
        }
        if (mobileEditText.getText().toString().trim().equalsIgnoreCase("")) {
            mobileEditText.setError("Cannot Be Blank");
            isFormContentValid = false;
        } else if (!isValidCellPhone()){
            mobileEditText.setError("Invalid mobile number");
            isFormContentValid = false;
        } else {
            mobileEditText.setError(null);
        }
        if (profImgUri == null) {
            Toast.makeText(mContext, "Please select a profile image", Toast.LENGTH_SHORT).show();
            isFormContentValid = false;
        }

        if (isFormContentValid) {
            register();
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.prof_img:

                if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermission(REQUEST_STORAGE_PERMISSION);
                } else {
                    openImageChooser();
                }
                break;
            case R.id.start_btn:
                validation();
                break;
            default:
                break;
        }

    }

    @Override
    protected void onDestroy() {
        ViewUtils.hideProgressDialog(mContext, dlgProgress);
        super.onDestroy();
    }
}
