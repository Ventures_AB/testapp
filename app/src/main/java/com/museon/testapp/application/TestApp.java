package com.museon.testapp.application;

import android.app.Application;

/**
 * Created by Aiswarya on 03/03/18.
 */

public class TestApp extends Application{

    public static TestApp appContext;

    public static String TAG = "TESTAPP";

    @Override
    public void onCreate() {
        super.onCreate();

        appContext = this;
    }

}

