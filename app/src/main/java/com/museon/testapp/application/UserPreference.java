package com.museon.testapp.application;

import android.content.Context;
import android.content.SharedPreferences;

import com.museon.testapp.utils.AppLog;

/**
 * Created by Mohammed Shafeeq on 04/03/18.
 */

public class UserPreference {

    private static UserPreference instance = null;
    private SharedPreferences sharedPreferences;

    private String USER_PREFERENCE = "EDUXLITE_USERPREFERNECE";
    private String TAG = "USER PREFERENCE";

    public UserPreference() {
        sharedPreferences = null;
    }

    public static UserPreference getInstance() {
        if (instance == null) {
            instance = new UserPreference();
        }
        return instance;
    }

    public void saveMyNumber(Context context, String mobile) {
        if (context == null) {
            return;
        }
        try {
            if(sharedPreferences == null) {
                sharedPreferences = context.getSharedPreferences(USER_PREFERENCE, Context.MODE_PRIVATE);
            }
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("my_number", mobile);
            editor.commit();
        } catch (Exception ex) {
            AppLog.e(TestApp.TAG, TAG+ " error saveMyNumber: " + ex.getMessage());
        }
    }

    public String getMyNumber(Context context) {
        if (context == null) {
            return "";// default
        }
        if(sharedPreferences == null) {
            sharedPreferences = context.getSharedPreferences(USER_PREFERENCE, Context.MODE_PRIVATE);
        }
        String azureTag = sharedPreferences.getString("my_number", "");
        return azureTag;
    }
}
