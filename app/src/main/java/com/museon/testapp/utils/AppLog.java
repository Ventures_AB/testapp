package com.museon.testapp.utils;

import android.util.Log;

import com.museon.testapp.BuildConfig;

/**
 * Created by Aiswarya on 03/03/18.
 */

public class AppLog {

    public static void d(String tag, String msg) {
        if (BuildConfig.ENABLE_LOG) {
            Log.d(tag, msg);
        }
    }

    public static void e(String tag, String msg) {
        if (BuildConfig.ENABLE_LOG) {
            Log.e(tag, msg);
        }
    }
}
