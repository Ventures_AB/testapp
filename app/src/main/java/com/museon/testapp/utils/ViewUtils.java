package com.museon.testapp.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.museon.testapp.R;
import com.museon.testapp.application.TestApp;
import com.wang.avi.AVLoadingIndicatorView;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by Aiswarya on 04/03/18.
 */

public class ViewUtils {

    public static void setProfileImageTestApp(final String mImageURL, final ImageView mImageView, final AVLoadingIndicatorView mProgressBar) {
        try {

            if (mProgressBar == null) {
                mProgressBar.setVisibility(View.GONE);
            } else {
                mProgressBar.setVisibility(View.VISIBLE);
            }


            Glide.with(TestApp.appContext).load(mImageURL)
                    .thumbnail(1.0f)
                    .crossFade()
                    .fitCenter()
                    .skipMemoryCache(true)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .fitCenter()
                    .error(R.mipmap.profile)
                    .placeholder(R.mipmap.profile)
                    .bitmapTransform(new CropCircleTransformation(TestApp.appContext))
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            mProgressBar.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            mProgressBar.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(mImageView);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void setContactsImageTestApp(final String mImageURL, final ImageView mImageView) {
        try {

            Glide.with(TestApp.appContext).load(mImageURL)
                    .thumbnail(1.0f)
                    .crossFade()
                    .fitCenter()
                    .skipMemoryCache(true)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .fitCenter()
                    .error(R.mipmap.profile)
                    .placeholder(R.mipmap.profile)
                    .bitmapTransform(new CropCircleTransformation(TestApp.appContext))
                    .into(mImageView);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //App common progressbar
    public static ProgressDialog getAppCommonProgressBar(final Activity mActivity) {
        ProgressDialog dlgProgress = new ProgressDialog(mActivity);
        try {

            View progressView = LayoutInflater.from(mActivity).inflate(R.layout.progress_dialog, ViewUtils.getNullParent());
            dlgProgress.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            dlgProgress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dlgProgress.setCanceledOnTouchOutside(false);
            dlgProgress.setCancelable(false);
            dlgProgress.show();
            dlgProgress.setContentView(progressView);
            lockOrientation(mActivity);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return dlgProgress;

    }

    //Hide app progress bar if available
    public static void hideProgressDialog(Activity mActivity, ProgressDialog mProgressDialog) {
        try {
            if (mProgressDialog != null
                    && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
            unlockOrientation(mActivity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Show app progress bar
    public static void showProgressDialog(Activity mActivity, ProgressDialog mProgressDialog) {
        try {
            if (mProgressDialog != null && (!mProgressDialog.isShowing())) {
                mProgressDialog.show();
            }
            unlockOrientation(mActivity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Locks app current orientation  state
    public static void lockOrientation(Activity mActivity) {
        try {
            int rotation = mActivity.getWindowManager().getDefaultDisplay().getRotation();

            switch (rotation) {
                case Surface.ROTATION_180:
                    mActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT);
                    break;
                case Surface.ROTATION_270:
                    mActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE);
                    break;
                case Surface.ROTATION_0:
                    mActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                    break;
                case Surface.ROTATION_90:
                    mActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //UnLocks app current orientation  state
    public static void unlockOrientation(Activity mActivity) {
        mActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
    }

    //It returns null view group
    public static ViewGroup getNullParent() {
        return null;
    }
}
