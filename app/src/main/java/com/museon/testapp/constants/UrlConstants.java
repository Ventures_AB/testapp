package com.museon.testapp.constants;

import com.museon.testapp.BuildConfig;

/**
 * Created by Mohammed Shafeeq on 04/03/18.
 */

public class UrlConstants {

    public static String BASE_URL = BuildConfig.BASE_URL;

    public static String REGISTER_API = "reg";

    public static String CONTACTS_API = "profile";

    public static String PROFILE_API = "profile";

    public static String PROFILE_UPDATE_API = "upload";
}
