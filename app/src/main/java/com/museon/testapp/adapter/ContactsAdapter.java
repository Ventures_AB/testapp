package com.museon.testapp.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.museon.testapp.R;
import com.museon.testapp.model.Contacts;
import com.museon.testapp.service.RecyclerViewOnItemClickListener;
import com.museon.testapp.utils.ViewUtils;

import java.util.ArrayList;

/**
 * Created by Aiswarya on 03/03/18.
 */

public class ContactsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private Context mContext;
    private ArrayList<Contacts> mContacts;
    RecyclerViewOnItemClickListener mRecyclerViewOnItemClickListener;

    public ContactsAdapter(Context context, ArrayList<Contacts> contacts, RecyclerViewOnItemClickListener itemClickListener) {
        this.mContext = context;
        this.mContacts = contacts;
        this.mRecyclerViewOnItemClickListener = itemClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_contacts_item, parent, false);
        ItemViewHolder itemViewHolder = new ItemViewHolder(v);
        return itemViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
        Contacts contact = mContacts.get(position);

        itemViewHolder.nameText.setText(contact.getName());
        if (!contact.getProfile_pic().equalsIgnoreCase("")) {
            ViewUtils.setContactsImageTestApp(contact.getProfile_pic(), itemViewHolder.image);
        }
    }

    @Override
    public int getItemCount() {
        return mContacts.size();
    }

    private class ItemViewHolder extends RecyclerView.ViewHolder{

        public TextView nameText;
        public ImageView image;

        public ItemViewHolder(final View itemView) {
            super(itemView);

            nameText = (TextView) itemView.findViewById(R.id.name_txt);
            image = (ImageView) itemView.findViewById(R.id.img);

            Typeface typeface = Typeface.createFromAsset(mContext.getAssets(), "fonts/Ubuntu-R.ttf");
            nameText.setTypeface(typeface);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mRecyclerViewOnItemClickListener.onItemClick(itemView, getAdapterPosition());
                }
            });
        }
    }

    public void clear() {
        mContacts.clear();
        notifyDataSetChanged();
    }
}

